USE gestion_deportiva;

CALL crearUsuario('admin', 'admin', 1);
CALL crearUsuario('uno', 'uno', 2);
CALL crearUsuario('dos', 'dos', 2);

CALL crearEstadio('olimpico');
CALL crearEstadio('central');

CALL crearActividad('gym', 90.5, 1);
CALL crearActividad('foot', 90.5, 1);
CALL crearActividad('gym', 90.5, 2);
CALL crearActividad('natacion', 90.5, 2);

