<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
Route::get('/usuario', 'userController@menuUsuario');

//2 (el 1 es misma funcion que la 8 de admin)
Route::get('/verActividadesPrecio', 'userController@verActividadesPrecioView');
Route::post('/viendoActividadesPrecio', 'userController@verActividadesPrecio');
//3
Route::get('/verActividadesEstadio', 'userController@verActividadesEstadioView');
Route::post('/viendoActividadesEstadio', 'userController@verActividadesEstadio');
//4
Route::get('/verActividadesMatriculadas', 'userController@verMatriculas');
//5
Route::get('/inscribirse', 'userController@inscripcionView');
Route::post('/inscribiendo', 'userController@inscribir');
//6
Route::get('/desinscribirse', 'userController@desinscripcionView');
Route::post('/desinscribiendo', 'userController@desinscribir');

Route::get('/adminLogin', 'loginController@loginAdminView');
Route::post('/loguearAdmin', 'loginController@loguearAdmin');

Route::get('/usuarioLogin', 'loginController@loginUsuarioView');
Route::post('/loguearUsuario', 'loginController@loguearUsuario');

Route::get('/admin', 'adminController@menuAdmin');
//1
Route::get('/crearUsuario', 'adminController@crearUsuarioView');
Route::post('/creandoUsuario','adminController@crearUsuario');
//2
Route::get('/eliminarUsuario', 'adminController@eliminarUsuarioView');
Route::post('/eliminandoUsuario','adminController@eliminarUsuario');
//3
Route::get('/crearActividad', 'adminController@crearActividadView');
Route::post('/creandoActividad','adminController@crearActividad');
//4
Route::get('/eliminarActividad', 'adminController@eliminarActividadView');
Route::post('/eliminandoActividad','adminController@eliminarActividad');
//5
Route::get('/crearEstadio', 'adminController@crearEstadioView');
Route::post('/creandoEstadio','adminController@crearEstadio');
//6
Route::get('/eliminarEstadio', 'adminController@eliminarEstadioView');
Route::post('/eliminandoEstadio','adminController@eliminarEstadio');
//7
Route::get('/listarUsuarios', 'adminController@listarUsuarios');
//8
Route::get('/listarActividades', 'adminController@listarActividades');
//9
Route::get('/listarEstadios', 'adminController@listarEstadios');
});
