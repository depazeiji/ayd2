<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;

class adminController extends Controller
{

  public function menuAdmin(){
    return view('adminViews.index');
  }

  public function crearUsuarioView(){
    return view('adminViews.crearUsuario');
  }

  public function crearUsuario(){
    $validacion = DB::select('CALL getNUsuario(?);',array(Request::get('nombre')));
    if($validacion[0]->N == 0){
        DB::statement('CALL crearUsuario(?,?,?);',array(
            Request::get('nombre'),
            Request::get('password'),
            2
        ));
        return view('adminViews.index')->with('error','Usuario creado exitosamente.');
    }else{
      return view('adminViews.crearUsuario')->with('error','Usuario ya existe.');
    }
  }

  public function eliminarUsuarioView(){
    $usuarios = DB::select('CALL getUsuarios()');
    if(empty($usuarios)){
      return 'No hay usuarios.';
    }$opciones = array();
    foreach ($usuarios as $usuario) {
      $opciones[$usuario->id_usuario] = $usuario->nombre;
    }
    return view('adminViews.eliminarUsuario',array('opciones' => $opciones));
  }

  public function eliminarUsuario(){
    DB::statement('CALL eliminarUsuario(?);', array(
      Request::get('id')
    ));
    return view('adminViews.index')->with('error', 'Usuario eliminado.');
  }

  public function crearEstadioView(){
    return view('adminViews.crearEstadio');
  }

  public function crearEstadio(){
    $validacion = DB::select('CALL getNEstadio(?);',array(Request::get('nombre')));
    if($validacion[0]->N == 0){
        DB::statement('CALL crearEstadio(?);',array(
            Request::get('nombre')
        ));
        return view('adminViews.index')->with('error','Estadio creado exitosamente.');
    }else{
      return view('adminViews.crearEstadio')->with('error','Estadio ya existe.');
    }
  }

  public function eliminarEstadioView(){
    $estadios = DB::select('CALL getEstadios()');
    if(empty($estadios)){
      return 'No hay estadios.';
    }
    $opciones = array();
    foreach ($estadios as $estadio) {
      $opciones[$estadio->id_estadio] = $estadio->nombre;
    }
    return view('adminViews.eliminarEstadio',array('opciones' => $opciones));
  }

  public function eliminarEstadio(){
    DB::statement('CALL eliminarEstadio(?);', array(
      Request::get('id')
    ));
    return view('adminViews.index')->with('error', 'Estadio eliminado.');
  }

  public function crearActividadView(){
    $estadios = DB::select('CALL getEstadios()');
    $opciones = array();
    foreach ($estadios as $estadio) {
      $opciones[$estadio->id_estadio] = $estadio->nombre;
    }
    return view('adminViews.crearActividad',array('opciones' => $opciones));
  }

  public function crearActividad(){
        DB::statement('CALL crearActividad(?,?,?);',array(
            Request::get('nombre'),
            Request::get('precio'),
            Request::get('estadio')
        ));
        return view('adminViews.index')->with('error','Actividad creada exitosamente.');

  }

  public function eliminarActividadView(){
    $actividades = DB::select('CALL getActividades()');
    if(empty($actividades)){
      return 'No existen actividades.';
    }
    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ')';
    }
    return view('adminViews.eliminarActividad',array('opciones' => $opciones));
  }

  public function eliminarActividad(){
    DB::statement('CALL eliminarActividad(?);', array(
      Request::get('id')
    ));
    return view('adminViews.index')->with('error', 'Actividad eliminada.');
  }

  public function listarUsuarios(){
    $usuarios = DB::select('CALL getUsuarios()');
    if(empty($usuarios)){
      return 'No existen usuarios.';
    }
    $opciones = array();
    foreach ($usuarios as $usuario) {
      $opciones[$usuario->id_usuario] = $usuario->nombre;
    }
    return view('adminViews.listarUsuarios',array('opciones' => $opciones));
  }

  public function listarEstadios(){
    $estadios = DB::select('CALL getEstadios()');
    if(empty($estadios)){
      return 'No existen estadios.';
    }
    $opciones = array();
    foreach ($estadios as $estadio) {
      $opciones[$estadio->id_estadio] = $estadio->nombre;
    }
    return view('adminViews.listarEstadios',array('opciones' => $opciones));
  }

  public function listarActividades(){
    $actividades = DB::select('CALL getActividades()');
    if(empty($actividades)){
      return 'No existen actividades.';
    }
    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ') - Precio: ' . substr($actividad->precio, 0, strlen($actividad->precio)-2);
    }
    return view('adminViews.listarActividades',array('opciones' => $opciones));
  }


}
