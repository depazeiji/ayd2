<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;

class loginController extends Controller
{

  public function loginAdminView(){
    return view('pages.loginAdmin');
  }

  public function loguearAdmin(){
    $user = Request::get('user');
    $password = Request::get('password');

    if ($user == 'admin' && $password == 'admin'){

      Session::put('user_id', -1);
      //Request::session()->put('rol', 1);

          return redirect('/admin');

    }else{
      return view('pages.loginAdmin')->with('error','Usuario o password incorrectos. Intente de nuevo.');
    }
  }

  public function loginUsuarioView(){
    return view('pages.loginUsuario');
  }

  public function loguearUsuario(){
    $user = Request::get('user');
    $password = Request::get('password');

    $usuarios = DB::select('CALL getUsuarios();');
      $opciones = array();
      foreach ($usuarios as $usuario) {
        if($usuario->nombre == $user){
          if($usuario->password == $password){
            Session::put('user',$user);
            Session::put('rol', 2);
            Session::put('user_id', $usuario->id_usuario);
                return redirect('/usuario');
          }
          else{
            return view('pages.loginUsuario')->with('error','Password incorrecto. Intente de nuevo.');
          }
        }

      }
      return view('pages.loginUsuario')->with('error','Usuario incorrecto. Intente de nuevo.');

    }
}
