<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;

class userController extends Controller
{

  public function menuUsuario(){
    return view('userViews.index');
  }

  public function verActividadesPrecioView(){
    return view('userViews.actividadesPrecioForm');
  }

  public function verActividadesPrecio(){
    $actividades = DB::select('CALL verActividadesPrecio(?);',array(
      Request::get('precio')
    ));
    if(empty($actividades)){
      return 'No hay actividades menores al precio definido.';
    }$opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ') - Precio: ' . substr($actividad->precio, 0, strlen($actividad->precio)-2);
    }
    return view('userViews.actividadesPrecio',array('opciones' => $opciones));
  }

  public function verActividadesEstadioView(){
    $estadios = DB::select('CALL getEstadios()');
    if(empty($estadios)){
      return 'No hay estadios.';
    }$opciones = array();
    foreach ($estadios as $estadio) {
      $opciones[$estadio->id_estadio] = $estadio->nombre;
    }
    return view('userViews.actividadesEstadioForm',array('opciones' => $opciones));
  }

  public function verActividadesEstadio(){
    $actividades = DB::select('CALL verActividadesEstadio(?);',array(
      Request::get('id_estadio')
    ));
    if(empty($actividades)){
      return 'El estadio selecionado no tiene actividades.';
    }    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ') - Precio: ' . substr($actividad->precio, 0, strlen($actividad->precio)-2);
    }
    return view('userViews.actividadesEstadio',array('opciones' => $opciones));
  }

  public function verMatriculas(){
    $actividades = DB::select('CALL verMatriculas(?);',array(
      Session::get('user_id')
    ));
    if(empty($actividades)){
      return 'No tiene actividades matriculadas.';
    }
    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ') - Precio: ' . substr($actividad->precio, 0, strlen($actividad->precio)-2);
    }
    return view('userViews.matriculas',array('opciones' => $opciones));
  }

  public function inscripcionView(){
    $actividades = DB::select('CALL getActividades()');
    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ') - Precio: ' . substr($actividad->precio, 0, strlen($actividad->precio)-2);
    }
    return view('userViews.inscripcion',array('opciones' => $opciones));
  }

  public function inscribir(){
        DB::statement('CALL crearMatricula(?,?);',array(
          Session::get('user_id'),
            Request::get('id_actividad')
        ));
        return view('userViews.index')->with('error','Inscrito exitosamente.');

  }

  public function desinscripcionView(){
    $actividades = DB::select('CALL verMatriculas(?);',array(
      Session::get('user_id')
    ));
    $opciones = array();
    foreach ($actividades as $actividad) {
      $opciones[$actividad->id_actividad] = $actividad->nombre_actividad . '(' . $actividad->nombre_estadio . ')';
    }
    return view('userViews.desinscripcion',array('opciones' => $opciones));
  }

  public function desinscribir(){
        DB::statement('CALL eliminarMatricula(?,?);',array(
            Session::get('user_id'),
            Request::get('id_actividad')
        ));
        return view('userViews.index')->with('error','Desinscrito exitosamente.');

  }

}
