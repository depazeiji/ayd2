@extends('app')

@section('title')
	Actividades Estadio
@stop

@section('header')
	<h2>Ver actividades en determinado estadio</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3></h3>
<hr>
	{!! Form::open(['url' => 'viendoActividadesEstadio']) !!}
	{!! Form::label('Estadio:') !!}
	{!! Form::select('id_estadio',$opciones, array_values($opciones)[0]) !!}
		{!! Form::submit('Ver') !!}
	{!! Form::close() !!}


	@if (isset($error))
		{{ $error }}
	@endif

@stop
