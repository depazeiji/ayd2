@extends('app')

@section('title')
  Actividades Matriculadas
@stop

@section('header')
  <h2>Actividades matriculadas</h2>
  <hr>
@stop

@section('info')
  <p></p>
@stop

@section('body')
<h3>Desincribirse de:</h3>
{!! Form::open(['url' => 'desinscribiendo']) !!}
  {!! Form::select('id_actividad',$opciones, array_values($opciones)[0]) !!}
  {!! Form::submit('Desinscribirse') !!}
{!! Form::close() !!}



  @if (isset($error))
    {{ $error }}
  @endif

@stop
