@extends('app')

@section('title')
	Actividades Precio
@stop

@section('header')
	<h2>Ver actividades menores a un precio</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3>Menores a</h3>
<hr>
	{!! Form::open(['url' => 'viendoActividadesPrecio']) !!}
		{!! Form::text('precio') !!}
		{!! Form::submit('Ver') !!}
	{!! Form::close() !!}


	@if (isset($error))
		{{ $error }}
	@endif

@stop
