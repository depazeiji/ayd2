  @extends('app')

  @section('title')
  	User
  @stop

  @section('header')
  	<h2>Menu Usuario</h2>
    <hr>
  @stop

  @section('info')
  	<p></p>
  @stop

  @section('body')
  <h3>Opciones:</h3>
  	<li><a href="/listarActividades">Visualizar todas las actividades</a></li>
  	<li><a href="/verActividadesPrecio">Visualizar actividades que tengan un precio menor a cierta cantidad</a></li>
  	<li><a href="/verActividadesEstadio">Visualizar actividades que se impartan en un determinado estadio</a></li>
  	<li><a href="/verActividadesMatriculadas">Visualizar actividades matriculadas</a></li>
  	<li><a href="/inscribirse">Inscribirse a una actividad</a></li>
  	<li><a href="/desinscribirse">Quitarse de una actividad</a></li>
    <li><a href="/usuarioLogin">Salir</a></li>



  	@if (isset($error))
  		{{ $error }}
  	@endif

  @stop
