@extends('app')

@section('title')
	Inscripcion
@stop

@section('header')
	<h2>Inscribirse a Actividad</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3>Actividades:</h3>
<hr>
{!! Form::open(['url' => 'inscribiendo']) !!}
  {!! Form::select('id_actividad',$opciones, array_values($opciones)[0]) !!}
  {!! Form::submit('Inscribirse') !!}
{!! Form::close() !!}



	@if (isset($error))
		{{ $error }}
	@endif

@stop
