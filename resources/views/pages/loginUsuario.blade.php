@extends('app')

@section('title')
	Login Usuario
@stop

@section('header')
	<h2>Login Usuario</h2>

@stop

@section('body')
	<section class="box">
		<h2>Datos</h2>
		{!! Form::open(['url' => 'loguearUsuario']) !!}
			{!! Form::label('Nombre:') !!}
			{!! Form::text('user') !!}
			{!! Form::label('Password:') !!}
			{!! Form::password('password') !!}
			{!! Form::submit('Log in') !!}
		{!! Form::close() !!}

		@if (isset($error))
			{{ $error }}
		@endif

	</section>
@stop
