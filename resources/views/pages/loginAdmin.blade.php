@extends('app')

@section('title')
	Login Admin
@stop

@section('header')
	<h2>Login Admin</h2>

@stop

@section('body')
	<section class="box">
		<h2>Datos</h2>
		{!! Form::open(['url' => 'loguearAdmin']) !!}
			{!! Form::label('Nombre:') !!}
			{!! Form::text('user') !!}
			{!! Form::label('Password:') !!}
			{!! Form::password('password') !!}
			{!! Form::submit('Log in') !!}
		{!! Form::close() !!}

		@if (isset($error))
			{{ $error }}
		@endif

	</section>
@stop
