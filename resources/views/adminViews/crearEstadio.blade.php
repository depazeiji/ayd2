@extends('app')

@section('title')
	Crear Estadio
@stop

@section('header')
	<h2>Crear Estadio</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3>Creacion de Estadio</h3>
<hr>
	{!! Form::open(['url' => 'creandoEstadio']) !!}
		{!! Form::label('Nombre:') !!}
		{!! Form::text('nombre') !!}
		{!! Form::submit('Crear') !!}
	{!! Form::close() !!}


	@if (isset($error))
		{{ $error }}
	@endif

@stop
