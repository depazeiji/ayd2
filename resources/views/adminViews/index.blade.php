  @extends('app')

  @section('title')
  	Admin
  @stop

  @section('header')
  	<h2>Menu Administrador</h2>
    <hr>
  @stop

  @section('info')
  	<p></p>
  @stop

  @section('body')
  <h3>Opciones:</h3>
  	<li><a href="/crearUsuario">Crear Usuario</a></li>
  	<li><a href="/eliminarUsuario">Eliminar Usuario</a></li>
  	<li><a href="/crearActividad">Crear Actividad</a></li>
  	<li><a href="/eliminarActividad">Eliminar Actividad</a></li>
  	<li><a href="/crearEstadio">Crear Estadio</a></li>
  	<li><a href="/eliminarEstadio">Eliminar Estadio</a></li>
  	<li><a href="/listarUsuarios">Listar todos los usuarios</a></li>
  	<li><a href="/listarActividades">Listar todas las actividades</a></li>
  	<li><a href="/listarEstadios">Listar todos los estadios</a></li>
    <li><a href="/adminLogin">Salir</a></li>



  	@if (isset($error))
  		{{ $error }}
  	@endif

  @stop
