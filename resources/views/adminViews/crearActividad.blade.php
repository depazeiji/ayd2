@extends('app')

@section('title')
	Crear Actividad
@stop

@section('header')
	<h2>Crear Actividad</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3>Creacion de Actividad</h3>
<hr>
{!! Form::open(['url' => 'creandoActividad']) !!}
  {!! Form::label('Nombre:') !!}
  {!! Form::text('nombre') !!}
  {!! Form::label('Precio:') !!}
  {!! Form::text('precio') !!}
  {!! Form::label('Estadio:') !!}
  {!! Form::select('estadio',$opciones, array_values($opciones)[0]) !!}
  {!! Form::submit('Crear') !!}
{!! Form::close() !!}



	@if (isset($error))
		{{ $error }}
	@endif

@stop
