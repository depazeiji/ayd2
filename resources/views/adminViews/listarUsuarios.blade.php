@extends('app')

@section('title')
  Listar Usuarios
@stop

@section('header')
  <h2>Listar Todos los Usuarios</h2>
  <hr>
@stop

@section('info')
  <p></p>
@stop

@section('body')
<h3>Usuarios:</h3>
{!! Form::open() !!}
  {!! Form::select('id',$opciones, array_values($opciones)[0]) !!}

{!! Form::close() !!}



  @if (isset($error))
    {{ $error }}
  @endif

@stop
