@extends('app')

@section('title')
  Listar Estadios
@stop

@section('header')
  <h2>Listar Todos los Estadios</h2>
  <hr>
@stop

@section('info')
  <p></p>
@stop

@section('body')
<h3>Estadios:</h3>
{!! Form::open() !!}
  {!! Form::select('id',$opciones, array_values($opciones)[0]) !!}

{!! Form::close() !!}



  @if (isset($error))
    {{ $error }}
  @endif

@stop
