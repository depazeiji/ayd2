@extends('app')

@section('title')
	Eliminar Estadio
@stop

@section('header')
	<h2>Eliminar Estadio</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
	<h3>Eliminacion de Estadio</h3>
	<hr>

	{!! Form::open(['url' => 'eliminandoEstadio']) !!}
		{!! Form::label('Nombre:') !!}
		{!! Form::select('id',$opciones, array_values($opciones)[0]) !!}
		{!! Form::submit('Eliminar') !!}
	{!! Form::close() !!}

	@if (isset($error))
		{{ $error }}
	@endif

@stop
