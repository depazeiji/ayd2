@extends('app')

@section('title')
	Eliminar Usuario
@stop

@section('header')
	<h2>Eliminar Usuario</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
	<h3>Eliminacion de Usuario</h3>
	<hr>

	{!! Form::open(['url' => 'eliminandoUsuario']) !!}
		{!! Form::label('Nombre:') !!}
		{!! Form::select('id',$opciones, array_values($opciones)[0]) !!}
		{!! Form::submit('Eliminar') !!}
	{!! Form::close() !!}

	@if (isset($error))
		{{ $error }}
	@endif

@stop
