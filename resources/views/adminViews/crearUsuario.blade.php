@extends('app')

@section('title')
	Crear Usuario
@stop

@section('header')
	<h2>Crear Usuario</h2>
  <hr>
@stop

@section('info')
	<p></p>
@stop

@section('body')
<h3>Creacion de Usuario</h3>
<hr>
	{!! Form::open(['url' => 'creandoUsuario']) !!}
		{!! Form::label('Nombre:') !!}
		{!! Form::text('nombre') !!}
		{!! Form::label('Password:') !!}
		{!! Form::text('password') !!}
		{!! Form::submit('Crear') !!}
	{!! Form::close() !!}


	@if (isset($error))
		{{ $error }}
	@endif

@stop
