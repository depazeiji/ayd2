<!DOCTYPE HTML>
<!--
	Striped by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Gestion Deportiva - @yield('title')</title>
<header>
		<img src="/images/logo3.jpeg" style="width:1400px;height:240px">
</header>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Content -->
			<div id="content">
				<div class="inner">
					<!-- Post -->
						<article class="box post post-excerpt">
							<header>
								@yield('header')
							</header>
							@yield('body')
						</article>
				</div>
			</div>

			<!-- Copyright -->
				<ul id="copyright">
					<li>&copy; Copyright.</li><li>Hecho por: Eiji De Paz</li>
				</ul>
			<footer>

			</footer>
		<!-- Scripts -->
			<script src="/assets/js/jquery.min.js"></script>
			<script src="/assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="/assets/js/main.js"></script>
			@yield('links')

	</body>
</html>
