use gestion_deportiva;

DELIMITER $$

CREATE PROCEDURE verActividadesPrecio(precio double(18,4))
BEGIN
SELECT a.precio AS precio, a.id_actividad AS id_actividad, a.nombre AS nombre_actividad,
  e.nombre AS nombre_estadio
  FROM actividad a, estadio e
  WHERE a.id_estadio = e.id_estadio
  AND a.precio < precio;
END $$

CREATE PROCEDURE verActividadesEstadio(id_estadio int)
BEGIN
SELECT a.precio AS precio, a.id_actividad AS id_actividad, a.nombre AS nombre_actividad,
  e.nombre AS nombre_estadio
  FROM actividad a, estadio e
  WHERE a.id_estadio = e.id_estadio
  AND a.id_estadio = id_estadio;
END $$

CREATE PROCEDURE verMatriculas(id_usuario int)
BEGIN
SELECT a.precio AS precio, a.id_actividad AS id_actividad, a.nombre AS nombre_actividad,
  e.nombre AS nombre_estadio
  FROM actividad a, estadio e, usuario u, usuario_actividad ua
  WHERE ua.id_usuario = u.id_usuario
  AND ua.id_actividad = a.id_actividad
  AND a.id_estadio = e.id_estadio
  AND u.id_usuario = id_usuario;
END $$

CREATE PROCEDURE crearMatricula(usuario int, actividad int)
BEGIN
INSERT INTO usuario_actividad(id_actividad,id_usuario) VALUES(actividad, usuario);
END $$

CREATE PROCEDURE eliminarMatricula(usuario int, actividad int)
BEGIN
DELETE FROM usuario_actividad WHERE id_usuario=usuario AND id_actividad = actividad;
END $$

-- usuario

CREATE PROCEDURE crearUsuario(nombre varchar(60), password varchar(70), administrador int)
BEGIN
INSERT INTO usuario(nombre,password,administrador) VALUES(nombre,password,administrador);
END $$

CREATE PROCEDURE eliminarUsuario(user int)
BEGIN
DELETE FROM usuario WHERE id_usuario=user;
END $$

-- estadio

CREATE PROCEDURE crearEstadio(nombre varchar(60))
BEGIN
INSERT INTO estadio(nombre) VALUES(nombre);
END $$

CREATE PROCEDURE eliminarEstadio(estadio int)
BEGIN
DELETE FROM estadio WHERE id_estadio=estadio;
END $$

-- actividad

CREATE PROCEDURE crearActividad(nombre varchar(100), precio double(18,4), id_estadio int)
BEGIN
INSERT INTO actividad(nombre,precio,id_estadio) VALUES(nombre,precio,id_estadio);
END $$

CREATE PROCEDURE eliminarActividad(actividad int)
BEGIN
DELETE FROM actividad WHERE id_actividad=actividad;
END $$

CREATE PROCEDURE getUsuarios()
BEGIN
SELECT id_usuario, nombre, password FROM usuario
 WHERE nombre NOT LIKE 'admin';
END $$

CREATE PROCEDURE getEstadios()
BEGIN
SELECT id_estadio, nombre FROM estadio;
END $$

CREATE PROCEDURE getActividades()
BEGIN
SELECT a.precio AS precio, a.id_actividad AS id_actividad, a.nombre AS nombre_actividad,
  e.nombre AS nombre_estadio
  FROM actividad a, estadio e
  WHERE a.id_estadio = e.id_estadio;
END $$

CREATE PROCEDURE getNUsuario(name varchar(60))
BEGIN
SELECT COUNT(*) as N from usuario WHERE nombre = name;
END $$

CREATE PROCEDURE getNEstadio(name varchar(60))
BEGIN
SELECT COUNT(*) as N from estadio WHERE nombre = name;
END $$

DELIMITER ;
